module.exports = {
  theme: {
    fontFamily: {
      hero: ['PT Serif', 'serif'],
      nums: ['Alfa Slab One', 'cursive'],
    },
    extend: {
      colors: {
        primary0: '#3700B3',
        primary1: '#FFC62F',
        primary2: '#03DAC5',
      },
    },
    letterSpacing: {
      normal: '.025em',
      wide: '.05em',
      wider: '.1em',
      widest: '.25em',
    },
    minHeight: {
      "90vh": '90vh'
    },
  },
  variants: {},
  plugins: [require('@tailwindcss/typography')],
  purge: {
    enabled: false,
  },
};
