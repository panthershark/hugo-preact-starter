const purgecss = require('@fullhuman/postcss-purgecss');
const plugins = [

  require('postcss-import')({
    path: ['assets/css']
  }),
  require('tailwindcss')(require.resolve('./tailwind.config.js'))

];

plugins.push(
  purgecss({
    content: ['./**/*.ts', './**/*.tsx', './**/*.html'],
    defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || [],
    whitelist: ['markdown']
  })
);


plugins.push(require('autoprefixer'));

module.exports = { plugins };
