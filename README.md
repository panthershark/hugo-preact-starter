# Hugo Preact Starter

This is a POC leveraging [Hugo](https://gohugo.io/) as the development ecosystem. Started as an experiment, it looks like a pretty sweet build chain.

### Usage

```
npm install
hugo server -D
```

Open a browser to http://localhost:1313

