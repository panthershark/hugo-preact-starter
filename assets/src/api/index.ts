import * as params from '@params';

// todo: generate this entire lib from an open api spec
export interface Api {
  currentUser: () => Promise<FakeUser>;
}

export interface FakeUser {
  userId: number;
  authToken: string;
  displayName: string;
  photoUrl: string;
}

const FAKE_USER: FakeUser = {
  userId: Math.random() % 1000,
  authToken: 'kdsjfhfkjdshf',
  displayName: params.fakeDisplayName,
  photoUrl: 'avatar-demo.png',
};

export const api: Api = {
  currentUser: () =>
    new Promise((resolve) => {
      setTimeout(() => resolve(FAKE_USER), 800);
    }),
};
