import { Api, FakeUser } from '../api/index';

export type AppUser = Loading | Anonymous | Authenticated;

export enum UserType {
  Loading = 1,
  Anonymous,
  Authenticated,
}

interface Loading {
  userType: UserType.Loading;
}

interface Anonymous {
  userType: UserType.Anonymous;
}

export type Authenticated = { userType: UserType.Authenticated } & FakeUser;

export const currentUser = async (api: Api): Promise<AppUser> => {
  const u = await api.currentUser();

  return {
    ...u,
    userType: UserType.Authenticated,
  };
};
