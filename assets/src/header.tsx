import * as params from '@params';
import { h, FunctionComponent, FunctionalComponent } from 'preact';
import { UserType, AppUser } from './lib/auth';

enum MenuExpanded {
  Closed = 1,
  UserMenu,
}

interface MenuItem {
  title: string;
  href: string;
  selected: boolean;
}

export interface HeaderState {
  menuItems: MenuItem[],
  expanded: MenuExpanded;
}


export const defaultHeaderState: HeaderState = {
  menuItems: [
    {
      title: 'Home',
      href: '/',
      selected: true,
    },
    {
      title: 'Preact',
      href: 'https://preactjs.com/',
      selected: false,
    },
    {
      title: 'Hugo',
      href: 'https://gohugo.io/',
      selected: false,
    },
    {
      title: 'ESBuild',
      href: 'https://github.com/evanw/esbuild',
      selected: false,
    },
  ],
  expanded: MenuExpanded.Closed,
}

export const Header: FunctionComponent<HeaderState & { user: AppUser }> = ({ user, menuItems }) => (
  <header class="flex justify-between p-2 bg-white w-full">
    <a href="/" title={params.siteTitle}>
      <div class="flex h-12 space-x-4 justify-end">
        <img class="h-full" src="/logo.png" />
        <h3 class="my-auto">{params.siteTitle}</h3>
      </div>
    </a>
    <div class="flex h-12 space-x-4 justify-end">
      <ul class="flex justify-end">
        {menuItems.map(m => <li class="my-auto px-2"><MenuLink {...m} /></li>)}
      </ul>
      <UserMenu user={user} />

    </div>
  </header >
);

const MenuLink: FunctionalComponent<MenuItem> = ({ title, selected, href }) => (<a href={href} class={selected ? 'border-b-2' : ''}>{title}</a>)

const UserMenu: FunctionalComponent<{ user: AppUser }> = ({ user }) => {
  switch (user.userType) {
    case UserType.Anonymous:
      return <a class="btn" href="/login">SIGN IN</a>
    case UserType.Authenticated:
      const avatarStyle = {
        backgroundImage: `url(${user.photoUrl})`,
        backgroundSize: '75%',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
      }

      return <div class="flex border-l-2 px-4">
        <span class="w-10 h-10 rounded-full border" style={avatarStyle}></span>
        <span class="my-auto px-4">{user.displayName}</span>
      </div>;
    default:
      return <div class="flex"><span class="my-auto border-l-2 px-4 w-24"></span></div>;
  }
}
