import "preact/debug"; // remove this from production build
import { h, render, FunctionComponent } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import { Header, defaultHeaderState, HeaderState } from './header';
import { Api, api } from './api/index'
import { currentUser, AppUser, UserType } from './lib/auth';

export interface AppState {
    header: HeaderState,
    api: Api
}

export const Main: FunctionComponent<AppState> = ({ api, header }) => {
    const [user, setUser] = useState<AppUser>({ userType: UserType.Loading });

    useEffect(() => {
        currentUser(api).then(setUser);
    }, []);

    return (
        <Header {...header} user={user} />
    );
};

const root = document.getElementById('root');
const defaultAppState = {
    api, header: defaultHeaderState
}

if (root) {
    root.innerHTML = '';
    render(<Main {...defaultAppState} />, root);
} else {
    throw new Error('bad root');
}
